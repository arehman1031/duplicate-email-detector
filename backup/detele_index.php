<?php
require 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

$client = ClientBuilder::create()->build();

$index = "my_emails";
$type = "string";

$deleteParams = [
    'index' => $index
];
$response = $client->indices()->delete($deleteParams);
print_r($response);
