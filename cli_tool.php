<?php
require "vendor/autoload.php";
ini_set("max_execution_time", 0);
ini_set("memory_limit","2048M");
ini_set('upload_max_filesize', '2048M');
ini_set('post_max_size', '2048M');

use Elasticsearch\ClientBuilder;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as XlsxWriter;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx as XlsxReader;


class DuplicateEmailDetector
{
    private $index = "";
    private $type = "";
    private $reader = null;
    private $client =  null;
    private $new_rows = 0;

    private $new_spreadsheet = null;
    private $new_worksheet = null;
    private $row_num = 0;
    private $sub_file_num = 1;

    private $new_writer = null;
    private $new_excel_name = "";

    public $target_dir = "";
    public $max_rows_allowed = 999999;

    function __construct($index = "my_emails", $type = "string")
    {
        $this->index = $index;
        $this->type = $type;
        $this->target_dir = "Unique/";
        if(!file_exists($this->target_dir)) {
            mkdir($this->target_dir);
        }

        $this->client = ClientBuilder::create()->build();

        $this->reader = new XlsxReader();
    }

    public function saveMasterData($target_file)
    {
        $spreadsheet = $this->reader->load($target_file);
        $total_worksheets = $spreadsheet->getSheetCount();

        echo "\nstoring emails into master database...\n\n";

        for ($ii=0; $ii < $total_worksheets; $ii++) {
            $worksheet = $spreadsheet->getSheet($ii);
            $rows = $worksheet->toArray();

            echo "from sheet ".($ii + 1)."...\n\n";

            foreach ($rows as $key => $row) {
                $email = $row[0];
                if(!empty($email)) {
                    if(!$this->search($email)) {
                        $this->insertRow($email);
                        $this->new_rows++;
                        echo $this->new_rows.") ".$email."\n\n";
                    }
                }
            }
        }
    }

    public function generateFromMasterData($target_file)
    {
        $spreadsheet = $this->reader->load($target_file);
        $total_worksheets = $spreadsheet->getSheetCount();

        $this->makeNewSubSpreadSheet();

        echo "\ngenerating sheets...\n\n";

        for ($ii=0; $ii < $total_worksheets; $ii++) {
            $worksheet = $spreadsheet->getSheet($ii);
            $rows = $worksheet->toArray();
            echo "from sheet ".($ii+1)."...\n\n";
            foreach ($rows as $key => $row) {
                $email = $row[0];
                if(!empty($email)) {
                    if(!$this->search($email)) {
                        $this->addEmailToExcel($email);
                        echo $this->new_rows.") ".$email."\n\n";
                    }
                }
            }
        }
    }

    public function search($email)
    {
        try {
            $params = [
                "index" => $this->index,
                "type" => $this->type,
                "size" => 1,
                "body" => [
                    "query" => [
                        "match" => [
                            "email" => $email
                        ]
                    ]
                ]
            ];

            $responses = $this->client->search($params);
            $doc = $responses["hits"]["total"];
            if($doc > 0) {
                return ($responses["hits"]["hits"][0]["_source"]["email"] == $email);
            } else {
                return false;
            }
        } catch (Elasticsearch\Common\Exceptions\Missing404Exception $e) {
            return false;
        }
    }


    private function insertRow($email)
    {
        $params = [
            "index" => $this->index,
            "type" => $this->type,
            "refresh" => true,
            "body" => [
                "email" => $email
            ]
        ];

        $response = $this->client->index($params);
    }

    public function getNewRows()
    {
        return $this->new_rows;
    }

    public function run($target_file)
    {
        $this->deleteIndex();
        $spreadsheet = $this->reader->load($target_file);
        $total_worksheets = $spreadsheet->getSheetCount();

        $this->makeNewSubSpreadSheet();

        echo "\ngenerating sheets...\n\n";

        for ($ii=0; $ii < $total_worksheets; $ii++) {
            $worksheet = $spreadsheet->getSheet($ii);
            $rows = $worksheet->toArray();
            echo "from sheet ".($ii+1)."...\n\n";
            foreach ($rows as $key => $row) {
                $email = $row[0];
                if(!empty($email)) {
                    if(!$this->search($email)) {
                        $this->insertRow($email);
                        $this->addEmailToExcel($email);
                        echo $this->new_rows.") ".$email."\n\n";
                    }
                }
            }
        }
    }

    private function makeNewSubSpreadSheet()
    {
        $this->new_spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $this->new_worksheet = $this->new_spreadsheet->getActiveSheet();
        $this->row_num = 1;

        $this->new_writer = new XlsxWriter($this->new_spreadsheet);

        $this->new_excel_name = $this->target_dir.date("Y-F-d G-i-s ").$this->sub_file_num.".xlsx";
        $this->sub_file_num++;
    }

    private function addEmailToExcel($email)
    {
        $this->new_worksheet->getCell("A".$this->row_num)->setValue($email);
        $this->row_num++;
        $this->writeExcel();
        $this->new_rows++;
        $this->checkIfSheetEnded();
    }

    private function checkIfSheetEnded()
    {
        if($this->row_num > $this->max_rows_allowed) {
            $this->makeNewSubSpreadSheet();
        }
    }

    private function writeExcel()
    {
        $this->new_writer->save($this->new_excel_name);
        // $this->new_spreadsheet = $this->reader->load($this->new_excel_name);
        // $this->new_worksheet = $this->new_spreadsheet->getActiveSheet();
    }



    public function deleteIndex($index="")
    {
        try {

            $deleteParams = [
                "index" => $this->index
            ];

            $response = $this->client->indices()->delete($deleteParams);

            return $response["acknowledged"];
        } catch (Elasticsearch\Common\Exceptions\Missing404Exception $e) {
            return 0;
        } catch (Elasticsearch\Common\Exceptions\NoNodesAvailableException $e) {
            die("Run elasticsearch instance please!!!");
        }
    }
}
